import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'; 

@Injectable({
  providedIn: 'root'
})
export class RestAuthService {

  constructor(private http: HttpClient) { }

  login$() {
    return this.http.get('fake-url')
  }
}
