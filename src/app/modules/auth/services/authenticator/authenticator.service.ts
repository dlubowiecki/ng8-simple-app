import { Injectable } from '@angular/core';
import { RestAuthService } from 'src/rest/services/rest-auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticatorService {

  constructor(private restAuthService: RestAuthService) {
  }

  login() {
    return this.restAuthService.login$()
  }
}
