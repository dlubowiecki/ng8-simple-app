import { Injectable, ErrorHandler, NgZone } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';

import { Notificator } from '../../notifications/services/notificator/notificator.service';

@Injectable()
export class CustomErrorHandler implements ErrorHandler {

  constructor(private zone: NgZone, private notificator: Notificator) { }

  handleError(error: any): void { 
    error = error.rejection || error;
    if (error instanceof HttpErrorResponse) {
      this.zone.run(() => {
        this.notificator.showError({message: `Http error status: ${error.status}`});
      });
    } else {
      this.notificator.showError({message: 'Custom error'});
    } 
  }
}
