import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import {Notification} from '../../models/notification'
 
@Injectable({
  providedIn: 'root'
})
export class Notificator {
  private notifications$: BehaviorSubject<Notification[]>

  constructor() {
    this.notifications$ = new BehaviorSubject<Notification[]>([])
  }

  getNotifications$() {
    return this.notifications$.asObservable()
  }

  showError(n: Notification) {
    const notifications = this.notifications$.value;
    this.notifications$.next([...notifications, n])
  }
}
