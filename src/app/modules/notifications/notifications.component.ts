import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { Notificator } from './services/notificator/notificator.service';
import {Notification} from './models/notification'

@Component({
  selector: 'ng8-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit {
  notifications$: Observable<Notification[]>

  constructor(private notificator: Notificator) {
    this.notifications$ = this.notificator.getNotifications$()
  }

  ngOnInit() {
  }

}
