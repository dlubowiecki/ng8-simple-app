import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';

import {Notification} from '../../models/notification'

@Component({
  selector: 'ng8-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NotificationComponent implements OnInit {
  @Input() notification: Notification;

  constructor() { }

  ngOnInit() {
  }

}
