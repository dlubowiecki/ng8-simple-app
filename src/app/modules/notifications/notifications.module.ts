import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NotificationsComponent } from './notifications.component';
import { NotificationModule } from './components/notification/notification.module';

@NgModule({
  declarations: [NotificationsComponent],
  exports: [NotificationsComponent],
  imports: [
    CommonModule,
    NotificationModule
  ]
})
export class NotificationsModule { }
