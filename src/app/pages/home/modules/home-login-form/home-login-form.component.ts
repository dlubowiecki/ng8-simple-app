import { Component, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms'; 

@Component({
  selector: 'ng8-home-login-form',
  templateUrl: './home-login-form.component.html',
  styleUrls: ['./home-login-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomeLoginFormComponent  {
  @Output() submit: EventEmitter<void>;
  @Output() login: EventEmitter<void>;
  form: FormGroup;

  constructor(private fb: FormBuilder) {
    this.submit = new EventEmitter();
    this.login = new EventEmitter();
    this.buildForm();
  } 

  private buildForm(): void {
    this.form = this.fb.group({
      login: ['']
    });
  }

  onSubmit() {
    this.submit.emit();
  }

  onLogin() {
    this.login.emit();
  }

  onError() {
    throw new Error('custom error')
  }
}
