import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomePageComponent } from './home-page.component';
import { HomeLoginFormModule } from './modules/home-login-form/home-login-form.module';


@NgModule({
  declarations: [HomePageComponent],
  imports: [
    CommonModule, 
    HomeRoutingModule,
    HomeLoginFormModule
  ]
})
export class HomeModule { }
