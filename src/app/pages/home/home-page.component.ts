import { Component } from '@angular/core'; 
import { OAuthService } from 'angular-oauth2-oidc';

import { AuthenticatorService } from 'src/app/modules/auth/services/authenticator/authenticator.service';

@Component({ 
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent  {
  constructor(private oauthService: OAuthService, private authenticatorService: AuthenticatorService){}

  async onLogin() {
    console.log('login'); 
    return await this.oauthService.fetchTokenUsingPasswordFlow('', '');
  }

  onSubmit() {
    console.log('submit');
    this.authenticatorService.login().subscribe()
  }
}
