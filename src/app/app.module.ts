import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, ErrorHandler } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { OAuthModule } from 'angular-oauth2-oidc';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component'; 
import { CustomErrorHandler } from './modules/errors/classes/custom-error-handler';
import { NotificationsModule } from './modules/notifications/notifications.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    OAuthModule.forRoot(),
    AppRoutingModule,
    NotificationsModule
  ],
  providers: [{provide: ErrorHandler, useClass: CustomErrorHandler}],
  bootstrap: [AppComponent]
})
export class AppModule { }
