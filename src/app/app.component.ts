import { Component } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';

import { authConfig } from './modules/auth/config/oauth-config';

@Component({
  selector: 'ng8-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent { 
  constructor(private oauthService: OAuthService) {
    this.configure();
  }

  private configure() {
    this.oauthService.configure(authConfig); 
    this.oauthService.loadDiscoveryDocumentAndTryLogin();
  }
}
